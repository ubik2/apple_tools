#!/bin/sh

function prettyToRaw {
    regex="^([0-9]+)([kKMG]?)$"
    pretty=$1
    raw="unknown"
    if [[ $pretty =~ $regex ]]; then
        if [ ${BASH_REMATCH[2]} == "k" -o ${BASH_REMATCH[2]} == "K" ]; then
            raw=$(( ${BASH_REMATCH[1]} * 1024 ))
        elif [ ${BASH_REMATCH[2]} == "M" ]; then
            raw=$(( ${BASH_REMATCH[1]} * 1024 * 1024 ))
        elif [ ${BASH_REMATCH[2]} == "G" ]; then
            raw=$(( ${BASH_REMATCH[1]} * 1024 * 1024 * 1024 ))
        fi
    fi
    echo $raw
}

function rawToPretty {
    let raw=$1
    let g=$(( 1024 * 1024 * 1024 ))
    let m=$(( 1024 * 1024 ))
    let k=1024
    if (( raw > g )); then
        echo $(( raw / g ))"G"
    elif (( raw > m )); then
        echo $(( raw / m ))"M"
    elif (( raw > k )); then
        echo $(( raw / k ))"K"
    else
        echo $raw
    fi
}

function getCodecArgs {
    codec=$1
    case codec in
        "mp4a.40.5")
            echo "-c:a libfdk_aac -profile:a aac_he"
            ;;
        "avc1.42001f")
            echo "-c:v libx264 -profile:v baseline -level 3.1"
            ;;
        "avc1.4d001f")
            echo "-c:v libx264 -profile:v main -level 3.1"
            ;;
        "avc1.640028")
            echo "-c:v libx264 -profile:v high -level 4.1"
            ;;
    esac
}

if (( $# < 3 )); then
    echo "Usage: $0 <input file> <output folder> <output file>";
    exit
fi

input=$1 # ~/Documents/consent_orig.mp4
outputFolder=$2 # ~/Documents
outputFile=$3 # output.m3u8

vbitrate1=$(prettyToRaw 110k)
abitrate1=$(prettyToRaw 40k)
resolution1=640x360
vcodec1="avc1.42001f" # -c:v libx264 -profile:v baseline -level 3.1
acodec1="mp4a.40.5" # -c:a libfdk_aac -profile:a aac_he

vbitrate2=$(prettyToRaw 200k)
abitrate2=$(prettyToRaw 40k)
resolution2=640x360
vcodec2="avc1.4d001f" # -c:v libx264 -profile:v main -level 3.1
acodec2="mp4a.40.5" # -c:a libfdk_aac -profile:a aac_he

vbitrate3=$(prettyToRaw 600k)
abitrate3=$(prettyToRaw 40k)
resolution3=640x360
vcodec3="avc1.640028" # -c:v libx264 -profile:v high -level 4.1
acodec3="mp4a.40.5" # -c:a libfdk_aac -profile:a aac_he

# These settings are used for all the variants
hslArgs="-f hls -hls_list_size 0"
codecSpeed=slow

echo "#EXTM3U" > "${outputFolder}/${outputFile}"

fname=$(rawToPretty $(( vbitrate1 + abitrate1 )) )b_${resolution1}.m3u8
ffmpeg -y -i ${input} -preset ${codecSpeed} $(getCodecArgs ${acodec1}) -b:a ${abitrate1} $(getCodecArgs ${vcodec1}) -b:v ${vbitrate1} ${hslArgs} -s ${resolution1} "${outputFolder}"/"${fname}"
echo "#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=$(( vbitrate1 + abitrate1 )),RESOLUTION=${resolution1},CODECS=\"${vcodec1},${acodec1}\"" >> "${outputFolder}"/"${outputFile}"
echo "${fname}" >> "${outputFolder}"/"${outputFile}"

fname=$(rawToPretty $(( vbitrate2 + abitrate2 )) )b_${resolution2}.m3u8
ffmpeg -y -i ${input} -preset ${codecSpeec} $(getCodecArgs ${acodec2}) -b:a ${abitrate2} $(getCodecArgs ${vcodec2}) -b:v ${vbitrate2} ${hslArgs} -s ${resolution2} "${outputFolder}"/"${fname}"
echo "#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=$(( vbitrate2 + abitrate2 )),RESOLUTION=${resolution2},CODECS=\"${vcodec2},${acodec2}\"" >> "${outputFolder}"/"${outputFile}"
echo "${fname}" >> "${outputFolder}"/"${outputFile}"

fname=$(rawToPretty $(( vbitrate3 + abitrate3 )) )b_${resolution3}.m3u8
ffmpeg -y -i ${input} -preset ${codecSpeed} $(getCodecArgs ${acodec3}) -b:a ${abitrate3} $(getCodecArgs ${vcodec3}) -b:v ${vbitrate3} ${hslArgs} -s ${resolution3} "${outputFolder}"/"${fname}"
echo "#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=$(( vbitrate3 + abitrate3 )),RESOLUTION=${resolution3},CODECS=\"${vcodec3},${acodec3}\"" >> "${outputFolder}"/"${outputFile}"
echo "${fname}" >> "${outputFolder}"/"${outputFile}"

