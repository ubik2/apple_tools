Overview
========
Apple's new iTunes Connect based TestFlight lets developers add internal testers, who will receive prerelease builds of the application for testing.

This document is meant to describe the steps that need to be taken by a user with the iTunes Connect "Admin" role to add a new user for testing, and also describes the steps that the test user needs to take.

Administrator
-------------

* Login to log into iTunes Connect with an Admin or Agent account

![Login Page](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/login.png)

* Click on the "Users and Roles" icon

![Menu Page](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/menu.png)

* Click on the "+" icon to add a new user

![Users and Roles : iTunes Connect Users](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/usersandroles.png)

* Enter the new user's data, and click the "Next" button

![Add User - Basic](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/adduser1.png)

* Assign the new user the "Technical" role (the "Admin" role can also test)

![Add User - Roles](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/adduser2.png)

* Choose any notifications (generally none) and click "Save"

![Add User - Notifications](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/adduser3.png)

* You should end up on the confirmation page with a notification that an email has been sent (possibly in yellow if the email address already exists)

![Add User - Done](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/adduser4.png)


User
----

* At this point, the user gets an email invitation to join the team, and they need to click on the activate link to activate their account

![Invite Email : iTunes Connect](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/activateuser1.png)

* The user will need to enter some information, including their new password (this will be skipped for existing accounts). After entering this information, click "Save"

![New User - Basic](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/activateuser2.png)

* Accept the terms of service by clicking the checkbox, and clicking "Accept"

![New User - TOS](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/activateuser3.png)

* At this point, the user ends up in the iTunes Connect menu page, and is done with this phase.

![New User - Menu](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/activateuser4.png)


Administrator
-------------

* Login to log into iTunes Connect with an Admin or Agent account

![Login Page](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/login.png)

* Click on the "Users and Roles" icon

![Menu Page](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/menu.png)

* Click on the newly added user

![Users and Roles : iTunes Connect Users](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/usersandroles.png)

* From the user details page, click the "Internal Tester" toggle to enable testing, then click "Save"

![Users and Roles : iTunes Connect Users : Details](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/userdetail.png)

* Now go back to the main menu (from the iTunes Connect link in the top left), and select the "My Apps" icon

![Menu Page](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/menu.png)

* Select the app you want the user to test

![My Apps](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/myapps.png)

* Select the "Prerelease" tab to view builds

![My Apps : Versions](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/myapps_versions.png)

* Select the "Internal Testers" tab to add our new tester

![My Apps : Prerelease](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/myapps_prerelease.png)

* Check the new tester (and leave existing users checked), then click "Invite".

![My Apps : Prerelease : Internal Testers](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/myapps_prerelease_internaltesters.png)

* Confirm the invitation by clicking "Invite" again

![My Apps : Prerelease : Internal Testers : Confirm](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/myapps_prerelease_internaltesters_confirm.png)


User
----
This should be done on an iOS device with iOS version 8.0 or above.

* At this point, the user gets an email invitation to install the app, and they need to click on the "Open in TestFlight" link. This should be done on an iOS device.

![Invite Email : Application](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/app_invite.png)

* This will launch the app store, and attempt to install the TestFlight app. The user will need to click on "Install" (or the cloud icon)

![AppStore - TestFlight - Install](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/testflight_install.png)

* After installing the TestFlight app, the user should launch testflight, by clicking the "Open" button

![AppStore - TestFlight - Open](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/testflight_open.png)

* When the TestFlight application opens, it will prompt the user to install the app, and the user should click the "Install" button.

![TestFlight - App - Install](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/app_install.png)

* After installing, the user should click the "Open" button to launch the app

![TestFlight - App - Open](https://bitbucket.org/ubik2/apple_tools/raw/744c6d3/testflight/app_open.png)

* At last, the user should be able to test the application


